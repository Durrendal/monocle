LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
DESTDIR=/usr/bin

install-fnl:
	sed -i 'i\ i\#!/usr/bin/fennel' src/monocle
	install ./src/monocle.fnl -D $(DESTDIR)/monocle

compile-lua:
	fennel --compile src/monocle.fnl > src/monocle.lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/monocle.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/monocle.lua

install-lua:
	install ./src/monocle.lua -D $(DESTDIR)/monocle

compile-bin:
	cd ./src && fennel --compile-binary monocle.fnl monocle-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install ./src/monocle-bin -D $(DESTDIR)/monocle
