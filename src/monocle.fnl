;;Author: Will Sinatra <wpsinatra@gmail.com>
;;v0.1 | GPLv3

;;===== Libraries =====
(local curl (require "cURL"))

;;===== Variables =====
(global spectacle {})
(global alpfeed "https://pkgs.alpinelinux.org/packages?maintainer=Will+Sinatra")

(var xml "")
(var dotpath ".monoclerc")

;;===== Utility =====
(fn info []
  (print "Usage: monocle [-f short] [-s short]
Author: Will Sinatra <wpsinatra@gmail.com>
v0.1 | GPLv3

-f:    Provide raw xml.
-s:    Return only specific target."))

(fn split [val check]
  (if (= check nil)
      (do
        (local check "%s")))
  (local t {})
  (each [str (string.gmatch val (.. "([^" check "]+)"))]
    (do
      (table.insert t str)))
  t)

(fn setdotpath []
  (local envcheck (assert (io.popen "printf $HOME")))
  (local home (assert (envcheck:read "*a")))
  (set dotpath (.. home "/.monoclerc")))

(fn exists? [file]
  (local f (io.open file "r"))
  (if
   (= f nil)
   false
   (io.close f)))

(fn configure []
  (setdotpath)
  (if
   (= (exists? dotpath) false)
   (do
     (print ".monoclerc does not exist")
     (os.exit)))
  (each [line (io.lines dotpath)]
    (do
      (var dat (split line "+++"))
      (tset spectacle (. dat 1) (. dat 2)))))

(fn get [url]
  (local f (assert (io.popen (.. "curl -s -N " url))))
  (local f (f:read "*a"))
  (set xml f))

(fn parse [xmldat tag rets]
  (var f "")
  (local t {})
  (each [str (string.gmatch xmldat (.. "<" tag ">(.-)</" tag ">+"))]
    (do
      (table.insert t str)))
  (if
   (= rets 2)
   (set f (.. (. t 1) "\n" (. t 2)))
   (= rets 1)
   (set f (. t 2)))
  f)

(fn peek [v]
  (get v)
  (var title "")
  (var date "")
  (if
   (~= (string.find "git" v) nil)
   (do
     (set title (parse xml "title" 2))
     (set date (parse xml "pubDate" 1)))
   (do
     (set title (parse xml "title" 2))
     (set date (parse xml "updated" 1))))
  (print ";;==========;;")
  (print title)
  (print date)
  (print ";;==========;;"))

(fn monocle []
  (each [k v (pairs spectacle)]
    (do
      (peek v))))

(lambda main [?info ?targ]
  (if
   (= ?info nil)
   (do
     (configure)
     (monocle))
   (= ?info "-h")
   (info)
   (= ?info "--help")
   (info)
   (= ?info "-s")
   (do
     (configure)
     (peek (. spectacle ?targ)))
   (= ?info "-f")
   (do
     (configure)
     (get (. spectacle ?targ))
     (print xml))))

(main ...)
