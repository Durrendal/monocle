## Monocle:
A simple RSS/Atom ingestion script written in Fennel

## Build:
make compile-lua - compile fennel to lua script
make compile-bin - compile a static binary

### Install:
make install-fnl - install monocle.fnl to /usr/bin/monocle
make install-lua - install monocle.lua to /usr/bin/monocle
make install-bin - install monocle-bin to /usr/bin/monocle

## Configure:
monocle requires a .monoclerc file to exist in the users home directory
the .monoclerc file is used to store urls to rss/atom feeds

```
fennel+++https://github.com/bakpakin/Fennrl/releases.atom
sbcl+++https://github.com/sbcl/sbcl/releases.atom
```

## Usage:
```
domus:~/Development/monocle$ monocle -h
Usage: monocle [-f short] [-s short]
Author: Will Sinatra <wpsinatra@gmail.com>
v0.1 | GPLv3

-f:    Provide raw xml.
-s:    Return only specific target.
```

Simply run monocle, it will pull down configured atom/rss feeds, and then return the title and date of the last post.

## Purpose:
I keep getting behind updating my Alpine Linux packages, monocle serves as a simple and portable way to quickly grab the last release date of all of the packages I maintain so I can keep up a little better.

Long term I'd like to add better parsing, and a pull of the pkgs.alpinelinux.org data, so I can compare dates between the packages releases and the last build time of my packages in Alpine.